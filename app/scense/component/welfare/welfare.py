#coding:utf8
'''
Created on 2013-11-4

@author: ywq
'''

from app.mem.mPlayer import playerWelfare_m
from app.scense.tables import welfare_table,welfareOrder
from app.scense.core.language.language import Lg
from twisted.python import log
from app.db import player_welfaredb
from app.scense.nodeapp.messageshow import sendMessage


class Welfare(object):
    '''福利
    '''
    def __init__(self,owner):
        self.owner = owner
        self.pid=owner.id
        data=player_welfaredb.getByPid(self.pid) #角色已经领取的任务
        self.info={} #所有福利任务信息  key:任务类型id,value:{}  角色正在进行中的任务信息
        for row in data:
            pw=playerWelfare_m.getObj(row['id'])
            if not pw:
                self.info[row['typeid']]=row
            else:
                info=pw.get("data")
                self.info[info['typeid']]=info
    
    def updateProgress(self,typeid):
        '''领取完任务奖励，更新新的任务
        @param typeid: int 任务类型
        '''
        maxP=len(welfareOrder.get(typeid))-1 #此种任务类型的任务最大下标
        playerTaskInfo=self.info[typeid]  #福利任务进行中的信息
        progress=playerTaskInfo["progress"]  #角色福利任务的进度
        if progress+1<=maxP: #如果还有任务
            pobj=playerWelfare_m.getObj(playerTaskInfo['id'])
            pobj.update("progress", progress+1)
            pobj.update("state",0)
            pobj.update("counts",0)
            pobj.syncDB()
            playerTaskInfo['progress']+=1
            playerTaskInfo['state']=0
            playerTaskInfo['counts']=0
        else:#如果下面没有任务了
            pobj=playerWelfare_m.getObj(playerTaskInfo['id'])
            pobj.update("state", 2)
            pobj.syncDB()
            playerTaskInfo['state']=2
    
    def typeToWelfareId(self,typeid):
        '''通过任务类型获取福利任务表id'''
        #welfareOrder={} #key:福利任务类型 ，value:[welfare任务主键id,welfare任务主键id]
        playerTaskInfo=self.info[typeid]#福利任务进行中的信息
        progress=playerTaskInfo["progress"]#角色福利任务的进度
        worder=welfareOrder[typeid] #【welfare任务表主键id,welfare任务表主键id】
        welfareId=worder[progress] # welfare表主键id
        return welfareId
        
        
    
    def getShow(self,rtn):
        '''获取福利显示'''
        rtn.dlist=[]
        for item in self.info.values():
            typeid=item['typeid']     #任务类型id
            progress=item['progress'] #任务进度下标
            counts=item['counts']     #数量
            state=item['state']       #完成状态 0未完成 1可领取  2任务已经是最后一个了
            task=welfareOrder[typeid] #任务列表 [任务id,任务id]
            if  task and state!=2:
                wid=task[progress]     #任务主键id
                winfo=welfare_table.get(wid,None)
                if winfo:#任务信息
                    data={"state":state,"icontype":winfo['icontype'],"number":winfo['number'],"typeid":winfo['typeid'],"descriptions":winfo['descriptions']}
                    rtn.dlist.append(data)
                else:
                    log.err("player.welfare.getShow() wid:%d not exist"%wid)
            else:
                log.err("player.welfare.getShow() typeid:%d not exist"%typeid)
                
               
            
    def assessment(self,typeid,val):
        '''触发任务
        @param typeid: int 任务类型
        @param val: int 可以使通关的id,也可以是充值的数量，也可以是消费金币的数量 
        '''
        # welfare_table={}#福利配置表所有数据 {id:{}}
        # self.info={} #所有福利任务信息  key:任务类型id,value:{}  正在进行中的任务信息
        # welfareOrder={} #key:福利任务类型 ，value:[welfare任务主键id,welfare任务主键id]
        
        if not self.info.has_key(typeid): #如果此任务已经领取
            return
        data=self.info[typeid]
        welfareORderList=welfareOrder.get(typeid) #此种任务类型的任务id排列顺序
        wid=welfareORderList[data['progress']]#任务id
        info=welfare_table.get(wid,None)      #任务信息
        if info['val']>0 and val==info['val']:#特定id 并且一样
            pw=playerWelfare_m.getObj(data['id'])
            pw.update("state", 1)
            data["state"]=1
            pw.syncDB()
            sendMessage(1601, Lg().g(1015), [self.owner.did]) #福利任务完成,可以去领奖励啦 
        if info['total']>0:
            data['counts']+=val #增加累计数量
            if data['counts']>=info['total']:
                pw=playerWelfare_m.getObj(data['id'])
                pw.update("state", 1)
                data["state"]=1
                pw.syncDB()
                sendMessage(1601, Lg().g(1015), [self.owner.did]) #福利任务完成,可以去领奖励啦 
            else:
                pw=playerWelfare_m.getObj(data['id'])
                pw.update("counts", data['counts'])
                pw.syncDB()
    
    def receiveTask(self,typeid):
        '''领取任务
        @param typeid: int 福利任务类型id
        '''
        pw=self.info[typeid]  #福利任务进行中的信息
        if pw and pw["state"]==1: #如果任务可以领取奖励
            typeid=pw['typeid'] #任务类型
            progress=pw['progress'] #任务类型的进度
            taskMain=welfareOrder[typeid][progress]#任务表主键id
            taskinfo= welfare_table.get(taskMain,None) #任务表信息
            icontype=taskinfo['icontype'] #1金币 2银币
            number=taskinfo['number'] #数量
            if icontype==2:
                self.owner.addSilver(number)
            elif icontype==1:
                self.owner.addGold(number)
            self.updateProgress(typeid)
            return True
            #print "通知客户端领取成功"
        else:
            return False
            
            
            
            
        
            
            
        