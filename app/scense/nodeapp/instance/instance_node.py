#coding:utf8
'''
Created on 2013-10-8

@author: jt
'''
from firefly.server.globalobject import remoteserviceHandle
from app.scense.applyInterface import instance_app
from app import js

@remoteserviceHandle('gate')
def getMainInstance_1201(did,data):
    '''获取主线可征战副本id
    @param did: int 登陆动态id
    @param data: str json数据
    '''
    info=js.load(data)
    pid=info['pid']#角色id
    rsp=js.Obj()
    instance_app.getInstanceid(pid, rsp)
    return js.objstr(rsp)


@remoteserviceHandle('gate')
def joinInstance_1202(did,data):
    '''角色请求进入副本
    @param did: int 登陆动态id
    @param data: str json数据
    '''
    info=js.load(data)
    pid=info['pid']#角色id
    rsp=js.Obj()
    instance_app.joinInstance(pid, rsp)
    return js.objstr(rsp)

@remoteserviceHandle('gate')
def instanceFight_1203(did,data):
    '''角色在副本中战斗数值合法验证
    @param did: int 登陆动态id
    @param data: str json数据
    '''
    info=js.load(data)
#    rsp=js.Obj()
#    instance_app.verification(info, rsp)
#    return js.objstr(rsp)


@remoteserviceHandle('gate')
def closeInstance_1204(did,data):
    '''关闭、退出副本
    @param did: int 登陆动态id
    @param data: str json数据
    '''
    info=js.load(data)
    pid=info['pid']#角色id
    rsp=js.Obj()
    instance_app.closeInstance(pid, rsp)
    return js.objstr(rsp)
    
    
    
    
    